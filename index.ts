const nodeFetch  = require("node-fetch");
const arrayToCsv = require('array-to-csv')
const fs = require("fs")
const path = require("path")

const defaultFileName = "results.csv"

// get cli arguments
const cliArguments = process.argv.slice(2, process.argv.length)

let filterPos = cliArguments.indexOf("-f")

let fileNamePos = cliArguments.indexOf("-n")

let langPos = cliArguments.indexOf("-l")

let startString = ""
let fileName = defaultFileName


// languages to extract
const defaultLangs = ["en-us", "ja-jp"]

let LANGS = defaultLangs


// apply filter if valid
if (filterPos != -1 && cliArguments[(filterPos+1)] && (typeof cliArguments[(filterPos+1)] === 'string')) {
  startString = cliArguments[(filterPos+1)]
}

// use the file name specified in the cli if valid
if (fileNamePos != -1 && cliArguments[(fileNamePos+1)] && (typeof cliArguments[(fileNamePos+1)] === 'string')) {
  fileName = cliArguments[(fileNamePos+1)]
}

// use the file name specified in the cli if valid
if (langPos != -1 && cliArguments[(langPos+1)] && (typeof cliArguments[(langPos+1)] === 'string')) {
  let langsParms = cliArguments[(langPos+1)]
  LANGS = langsParms.split(",").map(l => l.trim())
}
console.log(LANGS)

//process.exit(0)


console.log(`\n CURRENT LANGUAGES : ["${LANGS.join('", "')}"] \n`)

let ALL_KEYS_URL = "http://titanapi-preprod.12thwonder.com/LocalizationAdmin/localizationService.ashx?method=GetResourceListHtml&ResourceSet=Text"
let GET_KEYS_INFOS_URL = "http://titanapi-preprod.12thwonder.com/LocalizationAdmin/localizationService.ashx?method=GetResourceItems"



async function main () {
  try {
    let ResourceIds: any = await nodeFetch(ALL_KEYS_URL)
                                .then(res => res.json())
                                .then(results => results/* .slice(0,10) */.map(k => k.ResourceId));

    let ressourceItemsRequests = ResourceIds.map(ResourceId => getInfos(ResourceId))
    let resourceItemsRequestsResults = await Promise.all(ressourceItemsRequests)
          .then(results => results.map((res: any) => res.json()))
  
    let ressourceItems = await  Promise.all(resourceItemsRequestsResults)

    let tableRows = ResourceIds.map((ResourceId: string, index: number) => processOneRessource(ResourceId, ressourceItems[index]))
  
    // filter here
    if (startString && startString.trim().length > 0) {
      tableRows = tableRows.filter(x => x[0].startsWith(startString.trim()))
    }
  
    let finalTable = computeTable(tableRows, LANGS)
  
  
    let CSVText = arrayToCsv(finalTable)
    
    let filePath = path.join(__dirname, `${fileName}.csv`)
    fs.writeFile(filePath, CSVText, (err) => {
      if (err) {
        console.log(`\n An error happened when trying to create the file "${filePath}" \n`)
        return console.log(err)
      }
      console.log(`\n The result in save under "${filePath}" \n`)
    })
  } catch(err) {
      console.log(`\n An error happened when trying to extract the values \n`)
      console.log(err)
  }
}

main().catch(e => console.log(e))
/*
let all: any = nodeFetch(ALL_KEYS_URL)
.then(res => res.json())
.then(results => results.map(k => k.ResourceId))
.then(ResourceIds => ResourceIds.slice(0,2).map(ResourceId => getInfos(ResourceId)))
.then(requests => Promise.all(requests))
.then((infos: any) => infos.map(res => res.json()))
.then(results => results[0]) */

//Promise.all([])
/* async function f() {
  let x = await all
  //let x: any = await Promise.all(await all)
  //console.log(x)
  ///return await Promise.all(x)
}

f().catch(e => console.log(e)) */

let processOneRessource = (resourceId: string, rawRessourceItems: any[]) => {
  let resourceItems = getLanguageValueMap(rawRessourceItems)
  return computeRow(resourceId, resourceItems, LANGS)
}

/* process() {
  getLanguageValueMap()
  computeRow
  computeTable()
} */

function getInfos (ResourceId) {
  var body = { ResourceId, ResourceSet: "Text"}
  //console.log(body)
  return nodeFetch(GET_KEYS_INFOS_URL, {
    method: 'POST',
    body:    JSON.stringify(body),
    headers: { 'Content-Type': 'application/json' },
  })
}

/*
input: getLanguageValueMap([
  { IsRtl: false,  ResourceList: null,  ResourceId: 'Admin_Admin',  Value: 'Admin',  Comment: null,  Type: '',  LocaleId: 'en-us',  ValueType: 0,  Updated: '2018-02-08T00:39:22.5342907Z',  ResourceSet: 'Text',  TextFile: null,  BinFile: null,  FileName: '' },
  { IsRtl: false, ResourceList: null, ResourceId: 'Admin_Admin', Value: '管理者', Comment: null, Type: '', LocaleId: 'ja-jp', ValueType: 0, Updated: '2018-02-08T00:39:22.5342907Z', ResourceSet: 'Text', TextFile: null, BinFile: null, FileName: '' },
  { IsRtl: false, ResourceList: null, ResourceId: null, Value: '', Comment: null, Type: null, LocaleId: '', ValueType: 0, Updated: '2018-02-08T00:39:22.5342907Z', ResourceSet: 'Text', TextFile: null, BinFile: null, FileName: null }
])
output: { 'en-us': 'Admin', 'ja-jp': '管理者', '': '' }
*/
let getLanguageValueMap = (resourceItems: any[]) => {
  let f = (acc, cur) => {
    acc[cur.LocaleId] = cur.Value
    return acc
  }

  return resourceItems.reduce(f, {} )
}

/*
input : computeRow("Admin_Admin", { 'en-us': 'Admin', 'ja-jp': '管理者', '': '' }, ["en-us", "ja-jp"])
output: [ 'Admin_Admin', 'Admin', '管理者' ]
*/
let computeRow = (resourceId: string, resourceItems: {[k: string]: string}, langs: string[]) => {
  let remaingColums = langs.map(lang => resourceItems[lang])
  return [resourceId, ...remaingColums]
}

/*
input : computeTable([ [ 'x', 'Admin', '管理者' ], [ 'x', 'Save', '管者' ] ],  ["en-us", "ja-jp"])
output: [ [ 'ResourceId', 'en-us', 'ja-jp' ],  [ 'x', 'Admin', '管理者' ],  [ 'x', 'Save', '管者' ] ]

*/
let computeTable = (rows: string[], langs: string[]) => {
  let firstRow = ["ResourceId", ...langs]
  return [firstRow, ...rows]
}
## Requirements
- node 6.6.0 or +

## Installation
```
  git clone project_url
```
```
  cd project_directory
```
```
  npm install // install dependencies
```
## Compilation
### Linux / Mac
```
  npm run compile // will compiled the index.ts file to index.js
```
### Windows
```
  ./node_modules/typescript/bin/tsc -p ./tsconfig.json
```

## Usage
### Arguments
  - ```-f``` : filter
  - ```-l``` : language list (comma seperated)
  - ```-n``` : the filename

### Examples

  ```
  node index.js -f "Admin_Lookup" -l "en-us, fr-fr, ja-jp" -n "Admin_Lookup"
  ```

- extract all keys
  ```
    node index.js // will generate "results.csv"
  ```
- extract only keys that start with a specific text
  ```
  node index.js -f "Admin_Lookup"
  ```
- extract only values of a specific language
  ```
  node index.js -l "en-us, fr-fr, ja-jp"
  ```
- specify the filename
  ```
  node index.js -n "filename" // will generate "filename.csv"
  ```

## Resulted file
A file with the name **"results.csv"** is created in the project directory containing the extracted values in case no filename is specified.

